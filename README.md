This plugin oversees the launch of CMake based builds.

It provides a user interface for configuring the following parameters.

1. Source directory
2. Build directory
3. CMake Generator
4. Cache file - to prepopulate cmake variables
5. Build command
6. Install command
7. Clean/Incremental build

It is a work in progress. Please feel free to enter issues in github if you would like any modifications.

